/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 12:55:06 by aantropo          #+#    #+#             */
/*   Updated: 2019/09/28 12:55:09 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_H
# define LEMIN_H

# include "../libft/includes/libft.h"

# define MIN_INT		-2148483648
# define MAX_DISTANCE	2147483647

typedef struct s_dlist	t_dlist;
typedef struct s_room	t_room;
typedef struct s_lemin	t_lemin;
typedef struct s_path	t_path;
typedef struct s_solut	t_solution;
typedef struct s_ant	t_ant;

struct					s_dlist
{
	void				*data;
	t_dlist				*next;
};

t_dlist					*ft_add_elem(void *data);
int						ft_list_push(t_dlist **head, void *data);
void					*ft_get_last_elem(t_dlist *head);
void					ft_delete_list(t_dlist **list);
int						is_list_have(t_dlist *list, void *data);

struct					s_room
{
	char				*name;
	int					x;
	int					y;
	int					is_visited;
	int					distance;
	t_dlist				*nbrns;
	t_room				*parent;
	int					is_busy;
};

int						ft_init_room(t_lemin *lemin, char *name, int x, int y);
t_room					*find_room_by_name(t_lemin *lemin, char *name);
t_room					*find_room_by_min_distance(t_dlist *visited,
							int is_depend_on_visited);
void					ft_delete_room(t_room *room);

struct					s_lemin
{
	int					ants;
	t_room				*start;
	t_room				*end;
	t_dlist				*rooms;
	t_solution			*solution;
	t_dlist				*pathes;
	char				*error_message;
	long int			lines_amount;
	int					is_show_lines_amount;
};

struct					s_path
{
	t_dlist				*rooms;
	long int			length;
};

struct					s_solut
{
	t_dlist				*pathes;
	long int			pathes_amount;
	float				efficiency;
};

struct					s_ant
{
	int					number;
	int					is_on_end;
	t_dlist				*room;
};

void					ft_exit(t_lemin *lemin);
t_lemin					*lemin_init(void);
void					reset_rooms(t_lemin *lemin);

int						parser(t_lemin *lemin, int *i);
int						parser_ants_amount(t_lemin *lemin);
int						parser_rooms(t_lemin *lemin, char *line, int i,
							int *is_room_error);
int						parser_links(t_lemin *lemin, char *line);

void					ft_delete_words(char **words);
void					reverse_list(t_dlist **head);
long int				ft_atol(char *string);
void					print_list(t_dlist *head);

int						ft_bfs(t_room *start, t_room *end, t_room *head);
void					update_visited(t_dlist *list, t_room *end,
							t_room *start);
t_path					*build_path(t_room *start, t_room *end);
int						add_path(t_lemin *lemin, t_path *path);

void					print_ants_movement(t_lemin *lemin);

#endif
