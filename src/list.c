/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 12:55:35 by aantropo          #+#    #+#             */
/*   Updated: 2019/09/28 12:55:39 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/lemin.h"

t_dlist	*ft_add_elem(void *data)
{
	t_dlist *new_elem;

	new_elem = (t_dlist *)malloc(sizeof(t_dlist));
	if (!new_elem)
		return (NULL);
	new_elem->data = data;
	new_elem->next = NULL;
	return (new_elem);
}

int		ft_list_push(t_dlist **head, void *data)
{
	t_dlist *tmp;

	if (!*head)
	{
		*head = ft_add_elem(data);
		if (!(*head))
			return (0);
	}
	else
	{
		tmp = *head;
		while (tmp->next != NULL)
			tmp = tmp->next;
		tmp->next = ft_add_elem(data);
		if (!tmp->next)
			return (0);
	}
	return (1);
}

void	ft_delete_list(t_dlist **list)
{
	t_dlist *head;
	t_dlist *tmp;

	head = *list;
	if (!head)
		return ;
	while (head)
	{
		tmp = head->next;
		free(head);
		head = NULL;
		head = tmp;
	}
	*list = NULL;
}

void	*ft_get_last_elem(t_dlist *list)
{
	t_dlist *head;

	head = list;
	if (head)
	{
		while (head->next != NULL)
			head = head->next;
		return (head->data);
	}
	return (NULL);
}

int		is_list_have(t_dlist *list, void *data)
{
	t_dlist *head;

	head = list;
	while (head)
	{
		if (head->data == data)
			return (1);
		head = head->next;
	}
	return (0);
}
