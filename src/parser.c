/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 12:56:03 by aantropo          #+#    #+#             */
/*   Updated: 2019/09/28 12:56:04 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/lemin.h"

int	parser(t_lemin *lemin, int *i)
{
	if (!parser_ants_amount(lemin))
	{
		if (ft_strlen(lemin->error_message) == 0)
			lemin->error_message = "Error: bad amount of ants.";
		*i = 0;
		return (0);
	}
	else
		ft_printf("%d\n", lemin->ants);
	if (!parser_rooms(lemin, NULL, 1, i))
	{
		*i = !lemin->start || !lemin->end ? 0 : *i;
		if (ft_strlen(lemin->error_message) == 0)
			lemin->error_message = "Error: bad room signature.";
		return (0);
	}
	if (!lemin->start || !lemin->end)
	{
		*i = 0;
		lemin->error_message = "Error: there isn`t start or end room.";
		return (0);
	}
	return (1);
}

int	parser_ants_amount(t_lemin *lemin)
{
	char	*line;
	int		i;

	i = 0;
	while (i != -1 && get_next_line(0, &line) > 0)
	{
		if (line[0] && line[0] == '#' && ft_strcmp(line, "##start")
			&& ft_strcmp(line, "##end"))
		{
			ft_printf("%s\n", line);
			ft_strdel(line ? &line : NULL);
			continue;
		}
		i = -1;
		if (line[0] != '\0' && ft_str_is_numeric(line))
			lemin->ants = ft_atoi(line);
		ft_strdel(&line);
	}
	if (lemin->ants > 10000)
		lemin->error_message =
				"Error: can't proccess ant farm with more than 10000 ants.";
	ft_strdel(i >= 0 ? &line : NULL);
	return (lemin->ants > 0 && lemin->ants <= 10000);
}
