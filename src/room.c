/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   room.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 12:56:31 by aantropo          #+#    #+#             */
/*   Updated: 2019/09/28 12:56:33 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/lemin.h"

void	ft_fill_room(t_room *room, char *name, int x, int y)
{
	room->name = ft_strdup(name);
	room->x = x;
	room->y = y;
	room->is_busy = 0;
	room->is_visited = 0;
	room->distance = MAX_DISTANCE;
	room->nbrns = NULL;
	room->parent = NULL;
}

int		ft_init_room(t_lemin *lemin, char *name, int x, int y)
{
	t_room	*room;
	t_dlist	*tmp;

	tmp = lemin->rooms;
	while (tmp)
	{
		if (!ft_strcmp(((t_room *)(tmp->data))->name, name)
			|| (((t_room *)(tmp->data))->x == x
			&& ((t_room *)(tmp->data))->y == y))
		{
			lemin->error_message = "Error: duplicate room's name or coords.";
			return (0);
		}
		tmp = tmp->next;
	}
	room = (t_room *)malloc(sizeof(t_room));
	ft_fill_room(room, name, x, y);
	ft_list_push(&lemin->rooms, room);
	return (1);
}

t_room	*find_room_by_name(t_lemin *lemin, char *name)
{
	t_dlist *head;

	head = lemin->rooms;
	while (head)
	{
		if (!ft_strcmp(name, ((t_room *)head->data)->name))
			return ((t_room *)head->data);
		head = head->next;
	}
	return (NULL);
}

t_room	*find_room_by_min_distance(t_dlist *visited, int is_depend_on_visited)
{
	int		i;
	t_dlist	*v;
	t_room	*min;

	i = MAX_DISTANCE;
	v = visited;
	min = NULL;
	while (v)
	{
		if (((t_room *)v->data)->is_visited == 1 && is_depend_on_visited)
			;
		else if (((t_room *)v->data)->distance < i)
		{
			i = ((t_room *)v->data)->distance;
			min = (t_room *)v->data;
		}
		v = v->next;
	}
	return (min);
}

void	ft_delete_room(t_room *room)
{
	if (room)
	{
		free(room->name);
		room->name = NULL;
		if (room->nbrns)
			ft_delete_list(&room->nbrns);
		room->nbrns = NULL;
		free(room);
		room = NULL;
	}
}
