/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 12:55:20 by aantropo          #+#    #+#             */
/*   Updated: 2019/09/28 12:55:23 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/lemin.h"

void		ft_delete_words(char **words)
{
	int i;

	if (words)
	{
		i = -1;
		while (words[++i])
			ft_strdel(&words[i]);
		free(words);
	}
}

void		reverse_list(t_dlist **head)
{
	t_dlist *prev;
	t_dlist *current;
	t_dlist *next;

	prev = NULL;
	current = *head;
	while (current != NULL)
	{
		next = current->next;
		current->next = prev;
		prev = current;
		current = next;
	}
	*head = prev;
}

void		print_list(t_dlist *head)
{
	t_dlist *list;

	list = head;
	while (list)
	{
		ft_printf("%s  ", ((t_room *)list->data)->name);
		list = list->next;
	}
	ft_printf("\n");
}

long int	ft_atol(char *string)
{
	long int		number;
	unsigned int	digit;
	int				sign;

	sign = 0;
	number = 0;
	digit = 0;
	if (*string == '-')
	{
		sign++;
		string++;
	}
	while (digit <= 9 && *string)
	{
		digit = *string - '0';
		number = number * 10 + digit;
		string++;
	}
	return (sign ? -number : number);
}
