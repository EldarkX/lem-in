/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_solution.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 14:26:59 by aantropo          #+#    #+#             */
/*   Updated: 2019/09/28 14:27:00 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/lemin.h"

int		all_ants_is_end(t_ant *ants, int ants_number)
{
	int ant;

	ant = -1;
	while (++ant < ants_number)
	{
		if (!ants[ant].is_on_end)
			return (0);
	}
	return (1);
}

void	move_ant(t_lemin *lemin, t_ant *ant, int is_new)
{
	if (!is_new)
	{
		((t_room *)ant->room->data)->is_busy = 0;
		ant->room = ant->room->next;
	}
	ft_printf("L%d-%s ", ant->number, ((t_room *)ant->room->data)->name);
	((t_room *)ant->room->data)->is_busy = 1;
	if (((t_room *)ant->room->data) == lemin->end)
		ant->is_on_end = 1;
}

void	movement_proccess(t_lemin *lemin, t_ant *ants, int ant)
{
	t_dlist	*path;

	path = lemin->solution->pathes;
	while (++ant < lemin->ants)
	{
		if (ants[ant].is_on_end)
			continue;
		if (ants[ant].room)
		{
			move_ant(lemin, &ants[ant], 0);
			continue;
		}
		while (path)
		{
			if (!((t_room *)((t_path *)path->data)->rooms->data)->is_busy)
			{
				ants[ant].room = ((t_path *)path->data)->rooms;
				move_ant(lemin, &ants[ant], 1);
				break ;
			}
			path = path->next;
		}
	}
}

void	print_ants_movement(t_lemin *lemin)
{
	t_ant	ants[lemin->ants];
	int		ant;

	ft_printf("\n");
	ant = -1;
	while (++ant < lemin->ants)
	{
		ants[ant].is_on_end = 0;
		ants[ant].number = ant + 1;
		ants[ant].room = NULL;
	}
	while (!all_ants_is_end(ants, lemin->ants))
	{
		movement_proccess(lemin, ants, -1);
		ft_printf("\n");
		lemin->lines_amount++;
		lemin->end->is_busy = 0;
	}
	if (lemin->is_show_lines_amount)
		ft_printf("Number of lines: %ld\n", lemin->lines_amount);
}
