/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_links.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 12:56:10 by aantropo          #+#    #+#             */
/*   Updated: 2019/09/28 12:56:11 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/lemin.h"

static int	linker(t_lemin *lemin, char *name1, char *name2)
{
	t_room	*room1;
	t_room	*room2;
	t_dlist	*tmp;

	room1 = find_room_by_name(lemin, name1);
	room2 = find_room_by_name(lemin, name2);
	if (room1 == NULL || room2 == NULL || room1 == room2)
		return (-1);
	tmp = room1->nbrns;
	while (tmp)
	{
		if (((t_room *)(tmp->data)) == room2)
			return (-1);
		tmp = tmp->next;
	}
	ft_list_push(&room1->nbrns, room2);
	ft_list_push(&room2->nbrns, room1);
	return (1);
}

static int	get_link(t_lemin *lemin, char *line)
{
	char	**ws;
	int		ws_amount;
	int		i;

	ws_amount = 0;
	ws = ft_strsplit(line, '-');
	while (ws[ws_amount])
		ws_amount++;
	if (ws_amount == 2)
		i = linker(lemin, ws[0], ws[1]);
	else
		i = -1;
	ft_delete_words(ws);
	return (i);
}

int			parser_links(t_lemin *lemin, char *line)
{
	int i;

	i = get_link(lemin, line);
	if (i != -1)
		ft_strdel(&line);
	while (i != -1 && get_next_line(0, &line) > 0)
	{
		ft_printf("%s\n", line);
		if (line[0] && line[0] == '#' && ft_strcmp(line, "##start")
			&& ft_strcmp(line, "##end"))
		{
			ft_strdel(&line);
			continue;
		}
		i = get_link(lemin, line);
		ft_strdel(&line);
	}
	ft_strdel(&line);
	if (i == -1)
		lemin->error_message = "Error: bad link signature.";
	return (i == -1 ? 0 : 1);
}
