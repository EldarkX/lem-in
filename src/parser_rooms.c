/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_rooms.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 12:56:17 by aantropo          #+#    #+#             */
/*   Updated: 2019/09/28 12:56:18 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/lemin.h"

static int	ft_check_coords(char *coord1, char *coord2)
{
	long int	i1;
	long int	i2;

	if (!coord1 || !coord2 || !ft_strlen(coord1) || !ft_strlen(coord2)
		|| ft_strlen(coord1) > 13 || ft_strlen(coord2) > 13)
		return (0);
	i1 = (coord1[0] == '-' ? 1 : 0);
	i2 = (coord2[0] == '-' ? 1 : 0);
	if (ft_str_is_numeric(coord1 + i1) && ft_str_is_numeric(coord2 + i2))
	{
		i1 = ft_atol(coord1);
		i2 = ft_atol(coord2);
	}
	else
		return (0);
	if (i1 > 2147483647 || i1 < MIN_INT || i2 > 2147483647 || i2 < MIN_INT)
		return (0);
	return (1);
}

static int	get_room(t_lemin *lemin, char *line)
{
	int		ws_amount;
	int		i;
	char	**ws;

	ws = ft_strsplit(line, ' ');
	ws_amount = 0;
	while (ws[ws_amount])
		ws_amount++;
	if (ws_amount == 3 && (ft_strlen(ws[0]) + ft_strlen(ws[1]) +
		ft_strlen(ws[2]) + 2 == ft_strlen(line)))
	{
		i = -1;
		while (i != -2 && ws[0][++i])
			i = (!ft_isprint(ws[0][i]) || ws[0][i] == '-' || ws[0][i]
				== '\t' || ws[0][0] == 'L' || ws[0][0] == '#') ? -2 : i;
		if (i > 0 && ft_check_coords(ws[1], ws[2]))
		{
			i = ft_init_room(lemin, ws[0], ft_atoi(ws[1]), ft_atoi(ws[2]));
			ft_delete_words(ws);
			return (!i ? 0 : 1);
		}
	}
	ft_delete_words(ws);
	return (0);
}

static int	get_room_after_command(t_lemin *lemin, char *line,
		int is_start, int is_end)
{
	ft_strdel(&line);
	if (get_next_line(0, &line) > 0)
	{
		ft_printf("%s\n", line);
		if (get_room(lemin, line))
		{
			ft_strdel(&line);
			if (is_start)
			{
				if (lemin->start)
					return (0);
				lemin->start = (t_room *)ft_get_last_elem(lemin->rooms);
			}
			else if (is_end)
			{
				if (lemin->end)
					return (0);
				lemin->end = (t_room *)ft_get_last_elem(lemin->rooms);
			}
			return (1);
		}
	}
	ft_strdel(&line);
	return (0);
}

static int	parse_command(t_lemin *lemin, char *line, int *is_room_error)
{
	int i;

	i = 1;
	if (!ft_strcmp(line, "##start"))
		i = get_room_after_command(lemin, line, 1, 0);
	else if (!ft_strcmp(line, "##end"))
		i = get_room_after_command(lemin, line, 0, 1);
	else
		ft_strdel(&line);
	*is_room_error = i;
	return (i);
}

int			parser_rooms(t_lemin *lemin, char *line, int i, int *is_room_error)
{
	while (get_next_line(0, &line) > 0 && i)
	{
		ft_printf("%s\n", line);
		if (line[0] && line[0] == '#' && (!line[1] ||
			(line[1] && line[1] != '#')))
		{
			ft_strdel(&line);
			continue;
		}
		if (i > 0 && ft_strchr(line, '-'))
		{
			i = parser_links(lemin, line);
			return (!i ? 0 : 1);
		}
		if (line[0] && line[0] == '#' && line[1] && line[1] == '#')
		{
			i = parse_command(lemin, line, is_room_error);
			continue ;
		}
		i = get_room(lemin, line);
		*is_room_error = i;
		ft_strdel(&line);
	}
	ft_strdel(&line);
	return (!i ? 0 : 1);
}
