/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 12:55:55 by aantropo          #+#    #+#             */
/*   Updated: 2019/09/28 12:55:57 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/lemin.h"

int		main(void)
{
	t_lemin	*lemin;
	int		i;

	i = 1;
	lemin = lemin_init();
	if (!parser(lemin, &i))
	{
		ft_printf("%s\n", lemin->error_message);
		if (!i)
			ft_exit(lemin);
	}
	lemin->start->distance = 0;
	i = 1;
	while (i && ft_bfs(lemin->start, lemin->end, NULL))
	{
		i = add_path(lemin, build_path(lemin->start, lemin->end));
		reset_rooms(lemin);
	}
	if (lemin->solution->pathes_amount > 0)
		print_ants_movement(lemin);
	else
		ft_printf("Error: there aren't any pathes from start to end.\n");
	ft_exit(lemin);
	return (0);
}

t_lemin	*lemin_init(void)
{
	t_lemin *lemin;

	lemin = (t_lemin *)malloc(sizeof(t_lemin));
	lemin->rooms = NULL;
	lemin->start = NULL;
	lemin->end = NULL;
	lemin->pathes = NULL;
	lemin->ants = 0;
	lemin->lines_amount = 0;
	lemin->is_show_lines_amount = 0;
	lemin->error_message = "\0";
	lemin->solution = (t_solution *)malloc(sizeof(t_solution));
	lemin->solution->pathes_amount = 0;
	lemin->solution->efficiency = 0;
	return (lemin);
}

void	reset_rooms(t_lemin *lemin)
{
	t_dlist *rooms;

	rooms = lemin->rooms;
	while (rooms)
	{
		((t_room *)rooms->data)->distance = MAX_DISTANCE;
		((t_room *)rooms->data)->is_visited = 0;
		rooms = rooms->next;
	}
	lemin->start->distance = 0;
}

void	ft_exit(t_lemin *lemin)
{
	t_dlist	*room;
	t_dlist *path;

	if (lemin)
	{
		room = lemin->rooms;
		while (room)
		{
			ft_delete_room((t_room *)room->data);
			room = room->next;
		}
		ft_delete_list(&lemin->rooms);
		path = lemin->pathes;
		while (path)
		{
			ft_delete_list(&((t_path *)path->data)->rooms);
			room = path->next;
			free(path->data);
			path = room;
		}
		ft_delete_list(&lemin->pathes);
		free(lemin->solution);
		free(lemin);
	}
	exit(0);
}
