/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pathfinder.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 12:56:25 by aantropo          #+#    #+#             */
/*   Updated: 2019/09/28 12:56:26 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/lemin.h"

void	change_visited(t_dlist *visited, t_dlist *nbrns, t_room *head)
{
	if (!is_list_have(visited, (t_room *)nbrns->data) &&
		((t_room *)nbrns->data)->parent == NULL)
	{
		((t_room *)nbrns->data)->distance = head->distance + 1;
		ft_list_push(&visited, (t_room *)nbrns->data);
	}
}

int		ft_bfs(t_room *start, t_room *end, t_room *head)
{
	t_dlist	*visited;
	t_dlist	*nbrns;

	visited = NULL;
	head = start;
	ft_list_push(&visited, head);
	while (head && head != end)
	{
		nbrns = head->nbrns;
		while (nbrns)
		{
			if (is_list_have(nbrns, end))
			{
				ft_delete_list(&visited);
				return (1);
			}
			change_visited(visited, nbrns, head);
			update_visited(visited, end, start);
			nbrns = nbrns->next;
		}
		head = find_room_by_min_distance(visited, 1);
		head = head == start ? NULL : head;
	}
	ft_delete_list(&visited);
	return (head == end);
}

void	update_visited(t_dlist *list, t_room *end, t_room *start)
{
	t_dlist	*head;
	t_dlist	*nbrns;
	int		is_visited_total;

	is_visited_total = 1;
	head = list;
	while (head)
	{
		if (((t_room *)head->data)->is_visited)
		{
			head = head->next;
			continue;
		}
		nbrns = ((t_room *)head->data)->nbrns;
		while (nbrns && is_visited_total)
		{
			if ((((t_room *)nbrns->data)->distance == MAX_DISTANCE
				&& !((t_room *)nbrns->data)->parent) ||
				(((t_room *)nbrns->data == end) && end->parent != start))
				is_visited_total = 0;
			nbrns = nbrns->next;
		}
		((t_room *)head->data)->is_visited = (is_visited_total ? 1 : 0);
		head = head->next;
	}
}

t_path	*build_path(t_room *start, t_room *end)
{
	t_room *tail;
	t_path *path;

	tail = end;
	path = (t_path *)malloc(sizeof(t_path));
	path->length = 0;
	path->rooms = NULL;
	while (tail != start)
	{
		ft_list_push(&path->rooms, tail);
		path->length++;
		tail->parent = find_room_by_min_distance(tail->nbrns, 0);
		tail = tail->parent;
	}
	reverse_list(&path->rooms);
	return (path);
}

int		add_path(t_lemin *lemin, t_path *path)
{
	float			efficiency;
	long int		ants_group;
	long int		steps_amount;
	static long int	pathes_amount = 0;

	pathes_amount++;
	ants_group = lemin->ants / pathes_amount;
	steps_amount = path->length + ants_group;
	if (!lemin->pathes || (lemin->pathes &&
		((t_path *)ft_get_last_elem(lemin->pathes))->length < path->length))
		steps_amount--;
	efficiency = (float)1 / (float)steps_amount;
	if (efficiency > ((t_solution *)lemin->solution)->efficiency)
	{
		ft_list_push(&lemin->pathes, path);
		((t_solution *)lemin->solution)->pathes = lemin->pathes;
		((t_solution *)lemin->solution)->efficiency = efficiency;
		lemin->solution->pathes_amount = pathes_amount;
		return (lemin->solution->pathes_amount >= lemin->ants
			|| path->length == 1 ? 0 : 1);
	}
	ft_delete_list(&path->rooms);
	free(path);
	return (0);
}
